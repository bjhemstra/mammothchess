//
//  Bishop.swift
//  MammothChess
//
//  Created by Bart-Jeroen on 07-07-17.
//  Copyright © 2017 Bart-Jeroen. All rights reserved.
//

import UIKit

class Bishop: UIChessPiece {
    
    init(frame: CGRect, color: UIColor, vc: ViewController) {
        super.init(frame: frame)
        
        if color == #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1){
            self.text = "♝"
        }
        else{
            self.text = "♗"
        }
        self.isOpaque = false
        self.textColor = color
        self.isUserInteractionEnabled = true
        self.textAlignment = .center
        self.font = self.font.withSize(36)
        
        vc.chessPieces.append(self)
        vc.view.addSubview(self)
    }
    
    func doesMoveSeemFine(fromIndex source: BoardIndex, toIndex dest: BoardIndex) -> Bool{
        
        if abs(dest.row - source.row) == abs(dest.col - source.col){
            return true
        }
        return false
    }
 
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
}

