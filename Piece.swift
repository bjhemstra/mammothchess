//
//  Piece.swift
//  MammothChess
//
//  Created by Bart-Jeroen on 07-07-17.
//  Copyright © 2017 Bart-Jeroen. All rights reserved.
//

import UIKit
protocol Piece {
    var x: CGFloat {get set}
    var y: CGFloat {get set}
    
}
