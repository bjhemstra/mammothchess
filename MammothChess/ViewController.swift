//
//  ViewController.swift
//  MammothChess
//
//  Created by Bart-Jeroen on 07-07-17.
//  Copyright © 2017 Bart-Jeroen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblDisplayTurnOUTLET: UILabel!
    @IBOutlet weak var lblDisplayCheckOUTLET: UILabel!
    @IBOutlet var panOUTLET: UIPanGestureRecognizer!
    
    var pieceDragged: UIChessPiece!
    var sourceOrigin: CGPoint!
    var destOrigin: CGPoint!
    static var SPACE_FROM_LEFT_EDGE: Int = 27
    static var SPACE_FROM_TOP_EDGE: Int = 173
    static var TILE_SIZE: Int = 40
    var myChessGame: ChessGame!
    var chessPieces: [UIChessPiece]!
    var isAgainstAI: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        chessPieces = []
        myChessGame = ChessGame.init(viewController: self)
        print("SINGLEPAYER: \(isAgainstAI)")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        pieceDragged = touches.first!.view as? UIChessPiece
        if pieceDragged != nil {
            sourceOrigin = pieceDragged.frame.origin
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if pieceDragged != nil{
            drag(piece: pieceDragged, usingGestureRecognizer: panOUTLET)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if pieceDragged != nil{
            
            let touchLocation = touches.first!.location(in: view)
            
            var x = Int(touchLocation.x)
            var y = Int(touchLocation.y)
            
            x -= ViewController.SPACE_FROM_LEFT_EDGE
            y -= ViewController.SPACE_FROM_TOP_EDGE
            
            x = (x / ViewController.TILE_SIZE) * ViewController.TILE_SIZE
            y = (y / ViewController.TILE_SIZE) * ViewController.TILE_SIZE
            
            x += ViewController.SPACE_FROM_LEFT_EDGE
            y += ViewController.SPACE_FROM_TOP_EDGE
            
            destOrigin = CGPoint(x: x, y: y)
            
            let sourceIndex = ChessBoard.indexOf(origin: sourceOrigin)
            let destIndex = ChessBoard.indexOf(origin: destOrigin)
            
            if myChessGame.isMoveValid(piece: pieceDragged, fromIndex: sourceIndex, toIndex: destIndex){
                
                myChessGame.move(piece: pieceDragged, fromIndex: sourceIndex, toIndex: destIndex, toOrigin: destOrigin)
                print("destIndex is \(destIndex)")
                
                //check if game is over
                if myChessGame.isGameOver(){
                    displayWinner()
                    return
                }
                if shouldPromotePawn(){
                    promptForPalmPromotion()
                }
                else{
                    resumeGame()
                }
                
                            }
            else{
                pieceDragged.frame.origin = sourceOrigin
            }
        }
    }
    
    func resumeGame(){
        // display Check, if any
        displayCheck()
        
        //change the turn
        myChessGame.nextTurn()
        
        //display turn on screen
        updateTurnOnScreen()
        
        //make AI move, if necessary
        if isAgainstAI == true && !myChessGame.isWhiteTurn{
            
            myChessGame.makeAIMove()
            print("AI: ------------------")
            
            if myChessGame.isGameOver(){
                displayWinner()
                return
            }
            if shouldPromotePawn(){
                promote(panw: myChessGame.getPawnToBePromoted()!, into: "Queen")
            }
            
            displayCheck()
            
            myChessGame.nextTurn()
            
            updateTurnOnScreen()
        }
    }
    
    func promote(panw pawnToBePromoted: Pawn, into pieceName: String){
        
        let pawnColor = pawnToBePromoted.color
        let pawnFrame = pawnToBePromoted.frame
        let pawnIndex = ChessBoard.indexOf(origin: pawnToBePromoted.frame.origin)
        
        myChessGame.theChessboard.remove(piece: pawnToBePromoted)
        
        switch pieceName {
        case "Queen":
            myChessGame.theChessboard.board[pawnIndex.row][pawnIndex.col] = Queen(frame: pawnFrame, color: pawnColor, vc: self)
        case "Knight":
            myChessGame.theChessboard.board[pawnIndex.row][pawnIndex.col] = Knight(frame: pawnFrame, color: pawnColor, vc: self)
        case "Rook":
                myChessGame.theChessboard.board[pawnIndex.row][pawnIndex.col] = Rook(frame: pawnFrame, color: pawnColor, vc: self)
        case "Bishop":
                    myChessGame.theChessboard.board[pawnIndex.row][pawnIndex.col] = Bishop(frame: pawnFrame, color: pawnColor, vc: self)
        default:
            break
        }
    }
    
    func promptForPalmPromotion(){
        if let pawnToPromote = myChessGame.getPawnToBePromoted(){
            
            let box = UIAlertController(title: "Pawn promotion", message: "Choose piece", preferredStyle: UIAlertController.Style.alert)
            
            box.addAction(UIAlertAction(title: "Queen", style: UIAlertAction.Style.default, handler: { action in
                self.promote(panw: pawnToPromote, into: action.title!)
                self.resumeGame()
            }))
            
            box.addAction(UIAlertAction(title: "Knight", style: UIAlertAction.Style.default, handler: { action in
                self.promote(panw: pawnToPromote, into: action.title!)
                self.resumeGame()
            }))
            
            box.addAction(UIAlertAction(title: "Rook", style: UIAlertAction.Style.default, handler: { action in
                self.promote(panw: pawnToPromote, into: action.title!)
                self.resumeGame()
            }))
            
            box.addAction(UIAlertAction(title: "Bishop", style: UIAlertAction.Style.default, handler: { action in
                self.promote(panw: pawnToPromote, into: action.title!)
                self.resumeGame()
            }))
            
            self.present(box, animated: true, completion: nil)
        }
    }
    
    func shouldPromotePawn() -> Bool{
        return (myChessGame.getPawnToBePromoted() != nil)
    }
    
    func displayCheck(){
        let playerChecked = myChessGame.getPlayerChecked()
        
        if playerChecked != nil{
            lblDisplayCheckOUTLET.text = playerChecked! + " is in check!"
        }
        else{
            lblDisplayCheckOUTLET.text = nil
        }
    }
    
    func displayWinner(){
        let box = UIAlertController(title: "Game over", message: "\(myChessGame.winner!) wins", preferredStyle: UIAlertController.Style.alert)
        
        box.addAction(UIAlertAction(title: "Back to main menu", style: UIAlertAction.Style.default, handler: {
            action in self.performSegue(withIdentifier: "backToMainMenu", sender: self)
        }))
        
        box.addAction(UIAlertAction(title: "Rematch", style: UIAlertAction.Style.default, handler: {
            (action) in
            //clear screen, chess pieces array, and board matrix
            for chessPiece in self.chessPieces{
                self.myChessGame.theChessboard.remove(piece: chessPiece)
            }
            
            //create new game
            self.myChessGame = ChessGame(viewController: self)
            
            //update labels with game status
            self.updateTurnOnScreen()
            self.lblDisplayCheckOUTLET.text = nil
            
        }))
        
        self.present(box, animated: true, completion: nil)
    }
    
    func updateTurnOnScreen(){
        lblDisplayTurnOUTLET.text = myChessGame.isWhiteTurn ? "White's turn" : "Black's turn"
        lblDisplayTurnOUTLET.textColor = myChessGame.isWhiteTurn ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    func drag(piece: UIChessPiece, usingGestureRecognizer gestureRecognizer: UIPanGestureRecognizer){
        
        let translation = gestureRecognizer.translation(in: view)
        piece.center = CGPoint(x: translation.x + piece.center.x , y: translation.y + piece.center.y)
        gestureRecognizer.setTranslation(CGPoint.zero, in: view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

